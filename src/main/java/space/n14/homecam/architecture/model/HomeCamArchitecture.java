package space.n14.homecam.architecture.model;

import com.structurizr.Workspace;
import com.structurizr.view.Shape;

import static com.structurizr.model.Tags.PERSON;
import static com.structurizr.model.Tags.SOFTWARE_SYSTEM;

public class HomeCamArchitecture {

    public Workspace getWorkspace() {
        var workspace = new Workspace("Getting Started", "This is a model of my software system.");
        var model = workspace.getModel();

        var user = model.addPerson("User", "A user of my software system.");
        var softwareSystem = model.addSoftwareSystem("Software System", "My software system.");
        user.uses(softwareSystem, "Uses");

        var views = workspace.getViews();
        var contextView = views.createSystemContextView(softwareSystem, "SystemContext", "An example of a System Context diagram.");
        contextView.addAllSoftwareSystems();
        contextView.addAllPeople();

        var styles = views.getConfiguration().getStyles();
        styles.addElementStyle(SOFTWARE_SYSTEM).background("#1168bd").color("#ffffff");
        styles.addElementStyle(PERSON).background("#08427b").color("#ffffff").shape(Shape.Person);

        return workspace;
    }
}
