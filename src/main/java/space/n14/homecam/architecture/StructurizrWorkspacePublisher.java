package space.n14.homecam.architecture;

import com.structurizr.api.StructurizrClient;
import lombok.SneakyThrows;
import space.n14.homecam.architecture.model.HomeCamArchitecture;

public class StructurizrWorkspacePublisher {

    @SneakyThrows
    public static void main(String[] args) {
        var workspace = new HomeCamArchitecture().getWorkspace();

        var apiKey = System.getenv("STRUCTURIZR_API_KEY");
        var secret = System.getenv("STRUCTURIZR_SECRET");
        var workspaceId = Long.parseLong(System.getenv("STRUCTURIZR_WORKSPACE_ID"));

        var structurizrClient = new StructurizrClient(apiKey, secret);
        structurizrClient.putWorkspace(workspaceId, workspace);
    }
}
